# multiplication V1.0



## The idea

My daugther had some issues getting her "dark green belt" at school.
What the heck is that ? My daughter's teacher, in order to motivate her pupils to learn their multiplication tables, has decided to implement a system of judo's belt.
Pupils have to answer correctly to 15 out of 20 given questions in a minute to get the associated belt. 
The fact is that my daughter took too much time to answer, she had to calculate, which is not what is expected here.

## The project

So I decided to give her a boost by implementing a little program that could help her work a certain range of tables in a certain amount of time.
The first page allow the choice of the tables involved and the amount of time to answer the questions.
Once the "Start" button is clicked, a test page is shown, with 20 random operations. A countdown starts according to the time chosen the page before. 
At the end of the countdown, or if the "submit" button is pressed, the result is shown.
When 75% of the answers are right, then the belt is won !
Good answers are framed with green whereas wrong ones are framed in red, with the solution.
When "Try again" is clicked, the same test is cleared and restarts.


## Potential improvements

It could be nice to add a control over the randomly chosen operations in order not to have one more than once.
It also could be cool to persist the results in a file or a database so that the child could see its progress.
The association between belt colors and tables involved could be offered. A dropdown list could allow the pupil to choose the belt it want to attempt and the tables would be automatically checked.
An analyse of the tables that cause wrong answers could offer the possibility to give some advice to the pupil.

## Screenshots

![image.png](./image.png)

![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

![image-3.png](./image-3.png)

