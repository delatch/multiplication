<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" type="text/css" rel="stylesheet"/>
    <title>Document</title>
</head>
<body>
<section>
    <H1>Liste des tables à travailler</H1>

    <form method="post" action="test.php">

        <div name="choice" class="tables">
            <?php

            $values = isset($_POST["tables"]) ? $_POST["tables"] : array();

            for ($i = 1; $i < 10; $i++) {
                if (in_array($i, $values)) {
                    $checked = true;
                } else {
                    $checked = false;
                }
                echo '<input type="checkbox" name="tables[]" value="' . $i . '"';
                if ($checked) {
                    echo 'checked';
                }

                echo '>Table des ' . $i . '</input><br>';
            }
            ?>
        </div>
        <br>
        <div class="time">
             Temps imparti <input type="text" name="time" size="2" value="60"/>
        </div><br>
        <input type="submit" class="button"/>
    </form>
</section>

</body>
</html>
