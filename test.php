<?php

$tables = isset($_POST["tables"]) ? $_POST["tables"] : array();
$questions = isset($_POST["questions"]) ? $_POST["questions"] : array();
$answers = isset($_POST["answers"]) ? $_POST["answers"] : array();

if (isset($_POST["cancel"]) && $_POST["cancel"] == "true") {
    $playAgain = true;
    $answers = array();
}
else{
    $playAgain = false;
}

$watchResults = (count($answers)>0 && !$playAgain);

$right_answers = 0;
$wrong_answers = 0;
$total_answers = 0;
$time = isset($_POST["time"]) ? $_POST["time"] : 60;

function get_random_table()
{
    global $tables;
    $index = rand(1, count($tables)) - 1;
    return $tables[$index];
}

function get_random_number_under_ten_or_equal()
{
    return rand(0, 9);
}

class operation
{
    public $op1;
    public $op2;
    public $label;
    public $name;
    public $answer;
    public $to_check = false;

    function __construct()
    {
        $this->op1 = get_random_table();
        $this->op2 = get_random_number_under_ten_or_equal();
        $this->post_construct();
    }

    function build_operation_from_name($name)
    {
        $this->op1 = explode("x", $name)[0];
        $this->op2 = explode("x", $name)[1];
        $this->to_check = true;
        $this->post_construct();
    }

    function post_construct()
    {
        $this->build_label();
        $this->build_name();
        $this->answer = $this->op1 * $this->op2;
    }

    function build_label()
    {
        $this->label = $this->op1 . " X " . $this->op2 . " = ";
    }

    function build_name()
    {
        $this->name = $this->op1 . "x" . $this->op2;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" type="text/css" rel="stylesheet"/>
    <script src="./js/multi.js"></script>
    <title>Multiplications</title>
</head>

<body onload="init(<?php if ($total_answers > 0) {
    echo "true";
} else {
    echo "false";
} ?>, <?= $time ?>)">

<?php
if (count($tables) == 0) {
    echo "<H1>Aucune table choisie !</H1>";
    echo "<a href='index.php'>retour</a>";
    die();
}
?>

<section>
    <div class='instructions'>
        Tu as <?= $time ?> secondes pour répondre à un maximum de questions.<br>
        Au bout d'une minute tes réponses seront évaluées !
    </div>


    <?php
    if (!$watchResults) {
    ?>
        <div id="timer">Temps restant</div>
    <?php
    };
    ?>
<div id="retour"><a href="index.php">Retour aux réglages</a></div>
    <div class='operation'>
        <table>
            <form method='post' name="valider">

                <?php
                for ($i = 1; $i <= 20; $i++) {
                    if ($i % 2 == 1) {
                        echo "<tr>\n";
                    }
                    echo "<td>\n";

                    $question = isset($questions[$i]) ? $questions[$i] : "";
                    $answer = isset($answers[$i]) ? $answers[$i] : "";

                    $op = new operation();
                    if ($question != "") {
                        $op->build_operation_from_name($question);
                    }

                    $class = "unchecked";
                    if ($op->to_check && !(isset($_POST["cancel"]) && $_POST["cancel"] == "true")) {
                        if ($op->answer == $answer) {
                            $class = "right";
                            $right_answers++;
                        } else {
                            $class = "wrong";
                            if ($answer != "")
                                $answer .= '/';

                            $answer .= $op->answer;
                            $wrong_answers++;
                        }
                        $total_answers++;
                    }

                    echo '<div class="operation">' . $op->label . '<input type="hidden" name="questions[' . $i . ']"
                                                                            value="' . $op->name . '"/>
                            <input type="text" class="' . $class . '" size="2" style="text-align: right;"
                             name="answers[' . $i . ']" value="' . $answer . '"/>
                          </div>
                        </td>';

                    if ($i % 2 == 0) {
                        echo "</tr>\n";
                    }
                }

                echo "\t</table>";

                for ($i = 0; $i < count($tables); $i++) {
                    echo "<input type='hidden' name='tables[]' value='" . $tables[$i] . "'/>\n";
                }
                ?>

                <br>
                <div class="buttons" style="margin: auto;">

                    <input id="submit_button" type="submit" class="button" onclick="disableSubmit()"/>
                    <input type="hidden" name="time" value="<?= $time ?>"/>
                    <input type="hidden" name="cancel" id="cancel" value="false"/>
                    <input id="cancel_button" type="button" class="button" value="Recommencer" onclick="cancelForm()"/>

                </div>
            </form>
    </div>

    <?php
    if ($total_answers > 0){
    ?>

    <div id='resultat' onclick="hideResult();">
        <script>showResult();</script>

        <br><br>Résultat : <br><br>


        <?PHP echo $right_answers . ' bonnes réponses sur ' . $total_answers;

        if (($right_answers / $total_answers) >= 0.75) {
            echo "<br>Tu as ta ceinture !";
        } else {
            echo "<br>Il faut encore travailler pour avoir ta ceinture !";
        }
        ?>
        <?php }

        ?>

    </div>
</section
</body>
</html>