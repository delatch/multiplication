var time = 60;
var initialized = false;
var nbAppels = 0;

function setTime(newTime) {
    time = newTime;
}

function validate() {
    this.document.getElementById("submit_button").click();
}

function init(alreadySubmited, timeInSecond = 60) {
    if (alreadySubmited) {
        hideTimer();
        return;
    }
    setTime(timeInSecond);
    this.document.body.getElementsByTagName("input").item(1).focus();
    // showTimer();
    initialized = true;
    // setTimeout(validate, timeInSecond * 1000);
}

function showResult() {
    this.document.getElementById("resultat").hidden = false;
    setTimeout(hideResult, 5 * 1000);
}

function hideResult() {
    this.document.getElementById("resultat").hidden = true;
}

var x = setInterval(function () {
        if (initialized) {
            nbAppels++;
            this.document.getElementById("timer").hidden = false;
            let seconde = 'secondes';

            if(time<=1){
                seconde = "seconde";
            }
            this.document.getElementById("timer").innerHTML = '<b>Temps restant : ' + time + ' ' + seconde + '</b>';
            time--;
            if (time < 0 && initialized) {
                this.document.getElementById("timer").innerHTML = '<b>Temps écoulé !</b>';
                validate();
            }
        } else {
            this.document.getElementById("timer").hidden = true;
        }
    }
    , 1000);


function hideTimer() {
    this.document.getElementById("timer").hidden = true;
}

function cancelForm() {
    this.document.getElementById("cancel").setAttribute("value", "true");
    this.document.getElementById("submit_button").disabled = false;
    this.document.getElementById("submit_button").click();
    // this.document.getElementById("submit_button").disabled = false;
}

function disableSubmit() {
    this.document.getElementById("submit_button").disabled = true;
    this.document.valider.submit();
}